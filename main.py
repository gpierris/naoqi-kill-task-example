from naoqi import ALProxy
import time


import random
import time
import threading
import math

global motionProxy

def executeTask(angleLists=[0,0,0,0,0, 0], speedFraction=0.01):
    
    global motionProxy

    pChainName = "LArm"
    
    #With the motionProxy.post.* it is non-blocking    
    taskID  = motionProxy.post.angleInterpolationWithSpeed(pChainName, angleLists, speedFraction)
    
    return taskID

def main():

    global motionProxy

    #You may also avoid holding a taskList by using the function getTaskList wheneven you want to know which tasks are running, but with this you are in control of the tasks in case you want to move the code to a different platform
    allTasks = []         
    robotIP = "localhost"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "Could not create proxy to ALMotion"
        print "Error was: ",e
        sys.exit(1)



    motionProxy.setStiffnesses("LArm", 1.0)
    
 

    #A really complex behavior that always sends a lot of commands...

    killPreviousTasks_BeforeStartingNewTask = True
    waitTasksToBeCompleted_BeforeStartingNewTask = False
        
        
    for i in range(10): #This will send 10 tasks
        
        angleLists = [math.sin(i*math.pi/10.0),math.sin(i*math.pi/5.0),0.1,0.1, 0, 0]
        taskID = executeTask(angleLists) #This is non-blocking
        allTasks.append(taskID) #In case we have good logic and the commands we send have to be executed in the future.

    
    
        runningTasks = sum( map( motionProxy.isRunning, allTasks) ) #apply the function motionProxy.isRunning to each element of allTasks, and sum them (True = 1, False =0)
        print 'We have: ', runningTasks, ' running tasks'
        
            
        if( waitTasksToBeCompleted_BeforeStartingNewTask ):
        
            #We block here until they finish
            while( sum( map( motionProxy.isRunning, allTasks) ) != 0 ):

                time.sleep(0.01)
                
            allTasks = [] # All should have finished so we clear the list
             
        else:
            '''That should work!'''
            #for task in motionProxy.getTaskList():
            #   motionProxy.killTask(task[1])
            
            '''and that should work!'''
            #map(motionProxy.killTask, allTasks)
            
            '''and that should work!'''
            #motionProxy.killTasksUsingResources(["LShoulderPitch"])
            #motionProxy.killTasksUsingResources(["LShoulderRoll"])
                        
            '''and that should work! Try to be a persistent serial killer, apparently naoqi thinks it killed the thread, but the robot still does the motion'''
            #for task in motionProxy.getTaskList():
            #   while(motionProxy.isRunning(task[1])):
            #       motionProxy.killTask(task[1])
            
            '''but only this works that kills everything :( ... Weird!'''
            motionProxy.killAll()
            allTasks = []   
            


if __name__=="__main__":
    main()
